﻿using ClayTablet.SC.Diagnostics;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Globalization;
using Sitecore.Layouts;
using Sitecore.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SitecoreConnectorSamples
{

    /**
     * To use this pipeline put following into CT3Translation.config:
     * <processor type="SitecoreConnectorSamples.DependentItems" method="getDependentItems" /> 
     **/
    public class DependentItems
    {
        /*
         * public class DependentItemsPipelineArgs {
              String ItemDatabase { get; }
              String ItemId { get; }
              String SourceLanguage { get; }
              String SourceVersion { get; }
              String[] TargetLanguages { get; }
              List<String> DependentItems { get; }
            }*/
        public void getDependentItems(ClayTablet.SC.Pipelines.DependentItemsPipelineArgs args)
        {
            CTTLog.Debug("[CustomPipeline] Invoking RecursiveDependentItemsFinalLayoutPipeline.getDependentItems");
            CTTLog.Debug("[CustomPipeline] args.ItemDatabase = " + args.ItemDatabase);
            CTTLog.Debug("[CustomPipeline] args.ItemId = " + args.ItemId);
            CTTLog.Debug("[CustomPipeline] args.SourceLanguage = " + args.SourceLanguage);
            CTTLog.Debug("[CustomPipeline] args.SourceVersion = " + args.SourceVersion);
            CTTLog.Debug("[CustomPipeline] args.DependentItems.Count = " + args.DependentItems.Count);
            Database db = Sitecore.Configuration.Factory.GetDatabase(args.ItemDatabase);
            ID itemID = new ID(args.ItemId);
            Language language = db.GetLanguages().Where(e => e.Name.Equals(args.SourceLanguage)).First();
            Sitecore.Data.Version version = Sitecore.Data.Version.Parse(args.SourceVersion);

            Item item = db.GetItem(itemID, language, version);

            CTTLog.Debug("[CustomPipeline] Item Path: " + item.Uri);

            args.DependentItems.AddRange(ShowDependentItems(item, language, db).Distinct());
        }

        /// <summary>
        /// Get the device item from a device name
        /// </summary>
        private static Sitecore.Data.Items.DeviceItem GetDeviceItem(Sitecore.Data.Database db, string deviceName)
        {
            return db.Resources.Devices.GetAll().Where(d => d.Name.ToLower() == deviceName.ToLower()).First();
        }

        private List<String> ShowDependentItems(Item item, Language language, Database db)
        {
            List<String> dependentItems = new List<string>();
            ResourceItems resources = new Sitecore.Resources.ResourceItems(db);
            DeviceItem[] devices = resources.Devices.GetAll();

            Sitecore.Layouts.LayoutDefinition layout = Sitecore.Layouts.LayoutDefinition.Parse(item[Sitecore.FieldIDs.FinalLayoutField]);

            CTTLog.Debug("[CustomPipeline] Is layout null: " + (layout == null ? "Yes" : "No"));

            foreach (var device in devices)
            {
                CTTLog.Debug("[CustomPipeline] Device: " + device.Name);

                List<RenderingReference> refs = new List<RenderingReference>();
                Sitecore.Data.Fields.LayoutField layoutField = item.Fields["__renderings"];
                var layoutRef = layoutField.GetReferences(GetDeviceItem(item.Database, device.Name));
                if (layoutRef != null)
                {
                    refs.AddRange(layoutRef);
                }

                Sitecore.Data.Fields.LayoutField finalLayoutField = item.Fields["__Final Renderings"];
                if (finalLayoutField != null)
                {
                    var layoutFinalRef = finalLayoutField.GetReferences(GetDeviceItem(item.Database, device.Name));
                    if (layoutFinalRef != null)
                    {
                        refs.AddRange(layoutFinalRef);
                    }
                }

                foreach (RenderingReference rendering in refs.ToArray())
                {
                    string datasource = rendering.Settings.DataSource;
                    CTTLog.Info("datasource: " + datasource);

                    if (!String.IsNullOrEmpty(datasource))
                    {
                        ClayTablet.SC.Diagnostics.CTTLog.Debug("[CustomPipeline] Found datasource : " + datasource + " for item " + item.DisplayName);
                        Item dependentItem = db.GetItem(datasource, language);

                        if (dependentItem != null)
                        {

                            dependentItems.Add(dependentItem.ID.ToString());
                            dependentItems.AddRange(ShowDependentItems(dependentItem, language, db));
                        }
                    }
                }
            }
            return dependentItems;
        }
    }
}
